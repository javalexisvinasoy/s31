// Simple Server

/*
	Instructions:

	- Import the http module using the required directive.
	- Create a variable port and assign it with the value of 3000.
	- Create a server using the createServer method that will listen in to the port provided above.
	- Console log in the terminal a message when the server is successfully running.
	- Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	- Access the login route to test if it’s working as intended.
	- Create a condition for any other routes that will return an error message.
	- Access any other route to test if it’s working as intended.

*/

//Code here:

const http = require('http')

const port = 3000

const server = http.createServer(function(request, response) {
	if (request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to login page!')
	} else { 
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found.")
	}
})

server.listen(port)

console.log(`Server now accesible at locahost: ${port}`)